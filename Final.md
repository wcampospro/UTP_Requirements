# Final

---
## **Submission**

A **Build must be ready**, No playing in the inspector or waiting for builds!

Nothing needs to be handed in for this assignment.  
We will be going around each team via Discord checking for the following:

At this stage, the game should be finalized.  
It shoudl have all gameplay mechanics and features in place, working properly and polished.  
The game should be balanced and it should have some nice player feedback already jiuced up (Particles, lights, cam-shake, sfx, etc...)

Remember part of this grade will be taken from GKBoards per student.

---
## **Art**

100% of all finalized artwork should be in the game and working:
- Background art
- Character art, models, animations
- Level art, props, items
- UI Screens and HUD
- All Particles and VFX

---
## **Audio**

100% of all Finalized sound assets should be in the game and working:
- SFX for all Core Mechanics and Key Features
- Music for each level
- UI SFX
- UI Music

---
## **Game**
100% of all core game mechanics and key features are to be fully implemented, coded and working correctly, also they should be balanced.
  
- 100% of all game levels fully designed and set-dressed
- 100% of all HUD functionality in place with final art
- UI Start Screen, Pause Menu and Game Over Screens:
    - Fully in game with finalized art
    - All interactable options included per screen
- The game should be 100% done at this point
    - there is still room to some minimal bugfixing

---
## **Systems** 

***This section is mostly for the GTs***

- An Editor tool in place and working properly at 100%
    - Should be working and polished
    - This is part of your assignments
    - **One per GT student**

- A Debug System in place and working at 100%
    - You must be able to make testing easier with this tool
    - You must be able to test 100% of core mechanics and key features with this tool
    - One per team
    - This tool must be fully polished by this point
    - This tool must be fully features and have all available testable options

---

After *Final* you will be done with the development of your game and can focus now and getting it ready for Itch.io.

The final grade will be taken from:
- Team Based: The Final presentation of your project.
- Per Student: The amount and quality of detail of all your Tasks/Cards in GKBoards.
