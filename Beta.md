# Beta

---
## **Submission**

A **Build must be ready**, No playing in the inspector or waiting for builds!

Nothing needs to be handed in for this assignment.  
We will be going around each team via Discord checking for the following:

It must have a full gameplay loop (you should be able to win and lose the game, if available).  
A *Beta* aims to be **feature complete**, with most of the artwork in place, things dont necessarily need to be balanced or 100% bug-free.

---
## **Art**

100% of all finalized artwork should be in the game and working, including but not limited to:
- Background art
- Character art, models, animations
- Level art, props, items
- UI elements
- Fist pass Particles and VFX

---
## **Audio**

80% of all Finalized sound assets should be in the game and working already implemented, including but not limited to:
- SFX for all Core Mechanics and Key Features
- Music for each level
- UI SFX
- UI Music
- The other 20% can be first pass, but should be there

---
## **Game**
100% of all core game mechanics and key features are to be fully implemented, coded and working correctly (Bugs allowed)

- Gym fully working properly
    - you should be able to test every core mechanic in the game, including new features and mechanics
  
- 90% of all game levels fully designed and set-dressed
    - No whiteboxes anymore
    - some of them can have placeholder art if you want
  
- 90% of all UI / HUD functionality in place
    - At least 90% should be using final UI Art
    - the rest can use placeholder art
  
- A Start Screen, Pause Menu and Game Over Screens:
    - Fully designed at least with some final art
    - all options included per screen
    - In place and wired up to work correctly in the game
  
- *If Applicable*: Player Respawn / game reset fully functioning
    - This should be testable in the Debug System

- The game should be 95% done at this point
    - there is still room to bugfix
    - there is still room to add particles
    - there is still room to add post processing
    - there is still room to add camera shake
    - there is still room to Jiuce-Up your game
    - there is still room to balance your game

---
## **Systems** 

***This section is mostly for the GTs***

- An Editor tool in place and working properly at 100%
    - Should be working and polished
    - This is part of your assignments
    - **One per GT student**

- A Debug System in place and working at 100%
    - You must be able to make testing easier with this tool
    - You must be able to test 100% of core mechanics and key features with this tool
    - One per team
    - This tool must be fully polished bu this point

---

After *Beta* you will be done with the development of your game and will focus on juicing up your game, polishing visual, polishing the feel, balancing your game and bugfixing.
