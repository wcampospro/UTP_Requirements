# Prototype

---
## **Submission**

A **Build must be ready**, No playing in the inspector or waiting for builds!

Nothing needs to be handed in for this assignment.
We will be going around each team via Discord checking for the following:

A prototype aims to test **All** _Key Features and Core Mechanics_ of the game, it must have a **fully working Gym** that allows for easy testing of these Features and Mechanics.
No Art or audio are needed in Game.


---
## **Art**

0% required


---
## **Audio**

0% required


---
## **Game**
100% of **All core game mechanics and key features are to be fully implemented**, coded and working ( Expected to be Buggy )

- Gym fully working properly
- you should be able to test every core mechanic and key feature in the game
- remember theyre only Key Feature and Core Mechanics, not nice to have's

---
## **Systems**

***This section is mostly for the GTs***

- A first pass Debug System in place and working at 50% of options
- You must be able to make testing easier with this tool
- You must be able to test 50% of core mechanics and key features with this tool
- One per team

---

After *Prototype* you will have a very clear idea on what you need to change, remove and add to your game design, features and mechanics. You will know if this game has a potentially fun to play future.

This is a Coding Heavy deliverable with a bit of LD needs, other disciplines should start gathering Inspiration, creating moodboards, or concept art, and other needed stuff for the game.
