# Alpha

---
## **Submission**

A **Build must be ready**, No playing in the inspector or waiting for builds!

Nothing needs to be handed in for this assignment.  
We will be going around each team via Discord checking for the following:

It must have a full gameplay loop (you should be able to win and lose the game, if available)
An alpha aims to test every single **Core Mechanic and Key Feature**, dont stress over stretch goals.

---
## **Art**

Around 2/3 (67%) of all finalized artwork should be in the game and working, including but not limited to:
- Background art
- Character art, models, textures, rigs, animations
- Level art, props, items, models, textures
- UI elements, sprites

---
## **Audio**

Around 2/3 (67%) of all **FirstPass** sound assets should be in the game and working already implemented, including but not limited to:
  - SFX for all Core Mechanics and Key Features
  - Music for each level
  - UI SFX
  - UI Music

---
## **Game**

- Gym fully working properly
  - You should be able to test every core mechanic in the gym
  
- 50% of all game levels fully designed and whiteboxed
  - they can have placeholder art if you want
  
- Around 2/3 (67%) of all Basic UI / HUD functionality in place with **placeholder art**
  
- A Start Screen, Pause Menu and Game Over Screens:
  - Fully designed at least with some **placeholder art**
  - In place and wired up to work correctly in the game
  
- 80% of all core game mechanics fully implemented and coded and working correctly (Bugs allowed)
  
- Input with keyboard and mouse and/or game controllers fully implemented and working
  
- *If Applicable*: Player Respawn / game reset fully functioning
  - This should be testable in the Debug System

---
## **Systems** 

***This section is mostly for the GTs***

- An Editor tool in place and working properly (at 75%)
  - Can be extended later, but should be working
  - This is part of your assignments
  - **One per GT student**

- A Debug System (at 75%)
  - It could the developer console made in class or it could be a different system
  - You must be able to make testing easier with this tool
  - You must be able to test at least 75% of core mechanics and key features with this tool
  - One per team

---

This might seem like a lot, but most of these things are either already done or have overlap with other sections in this document, we're just trying to be very explicit to help you out  plan your tasks and time.

After alpha you will be almost done with the development of your game and will mostly focus on polishing ideas and bugfixing.
