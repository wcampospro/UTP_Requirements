# Unity team project requirements

This document is intended to help you plan and prepare for the Unity Team Project.

These are the requirements for each deliverable:

- [Micro GDD](MicroGDD.md)
- [Prototype](Prototype.md)

- [Alpha](Alpha.md)

- [Beta](Beta.md)

- [Final](Final.md)
