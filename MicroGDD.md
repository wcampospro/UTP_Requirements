# MicroGDD

Remember this is a Micro GDD, not a full GDD.

These are the requirements for the Micro GDD document.

- It must be a Markdown file (.md)
- you can use the Readme.md in your repo or you can place it next to the Readme.md
- feel free to add images and gifs to your content
- Parts it must contain:
  - Elevator pitch
    - a simple explanation of the game core mechanics and key features
    - Remeber to start with the camera perspective
    - do not include story, just staging ideas
  - Game Concept
    - a slighly more detailed explanation of your game, you can include story in this part
  - Every Core Game Mechanic and Feature
  - File & Folder Structure and naming convention
    - simply the folder structure you desire to keep things organized in the project
    - and the naming convention for fils and folders
  - Coding Conventions
    - The coding convention to follow when programming this project
    - Code Naming convention
    - Brackets and indentation schemes
    - etc...

While a micro GDD, feel free to add more content and make it more complete like you have learn from other classes, this is a great opportunity to practice making good documentation too.
